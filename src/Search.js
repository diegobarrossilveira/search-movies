import React, { Component } from 'react';
import {TextField, IconButton} from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';

const API_KEY = '105ef61f';
const baseURL = `http://www.omdbapi.com/?apikey=${API_KEY}&plot=full&t=`;

export default class Search extends Component {
	constructor(props, context) {
    	super(props);
    	this.state = {movieTitle: '', movieInfo: {}};
    	this.handleTextChange = this.handleTextChange.bind(this);
    	this.searchMovie = this.searchMovie.bind(this);
    	this.fetchMovieData = this.fetchMovieData.bind(this);
	}

	handleTextChange = event => {this.setState({movieTitle: event.target.value});}
  
	searchMovie = () => {this.fetchMovieData(baseURL + this.state.movieTitle);}

	fetchMovieData = URL => {
    	fetch(URL)
    	.then(results => {return results.json()})
    	.then(movieJSON => {
				this.setState({movieInfo: movieJSON});
    	        this.props.callback(this.state.movieInfo);
              }
        )
    	.catch(fetchError => {console.error('Error:',fetchError)});
	}

	componentDidMount() {}

	componentWillUnmount() {}

	render() {
    	return(
	    	<div>
	    		<TextField
	     			placeholder="Movie name"
  	     			id="movie-search-field"
	     			onChange={this.handleTextChange}
	     			required={true}
	  			/>
	  			<IconButton onClick={this.searchMovie}>
            		<SearchIcon />
          		</IconButton>
			</div>
      );
	}
}
