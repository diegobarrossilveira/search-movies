import React, { Component } from 'react';
import {Card, CardActionArea, CardActions, CardContent, CardHeader, CardMedia, Collapse} from '@material-ui/core';
import {Button, IconButton, Typography} from '@material-ui/core';
import classnames from 'classnames';
import 'typeface-roboto';

const styles = theme => ({
  	card: {
		maxWidth: 400,
  	},
	media: {
		height: 0,
		paddingTop: '56.25%', //16.9
	},
	actions: {
		display: 'flex',
	},
	expand: {
		transform: 'rotate(0deg)',
		transition: theme.transitions.create('transform', {
	      		duration: theme.transitions.duration.shortest,
	    	}),
	    	marginLeft: 'auto',
	    	[theme.breakpoints.up('sm')]: {
	      		marginRight: -8,
	    	},
	},
	expandOpen: {
		transform: 'rotate(180deg)',
	},
	button: {
    	margin: theme.spacing.unit,
  	},
});

export default class SearchResults extends Component {
	constructor(props, context) {
    		super(props);
    		this.state = {expanded: false, buttonTitle: 'MORE INFO'};
	}

	handleExpandClick = () => {
		this.setState(previousState => ({expanded: !previousState.expanded, buttonTitle: 'LESS INFO'}));
    	this.state = {expanded: false};
	}

	handleExpandClick = () => {
		this.setState(previousState => ({expanded: !previousState.expanded}));
	};
  
	componentDidMount() {}

	componentWillUnmount() {}

	render() {
		let display;

		//Checks whether or not A movie has been searched (by checking whether the searchedMovie prop is an empty Object)
		if(Object.keys(this.props.searchedMovie).length === 0 && (this.props.searchedMovie).constructor === Object)
			display = <div></div>;
		else if(this.props.searchedMovie.Response === 'True')
				display =  
					<Card className = {styles.card}>
						<CardHeader
							title={this.props.searchedMovie.Title}
							subheader={this.props.searchedMovie.Year}
						/>
						<img
							src={this.props.searchedMovie.Poster}
							title="Movie Poster"
						/>
						<CardActions className={styles.actions} disableActionSpacing>
							<Button 
								className={classnames(styles.expand, {
									[styles.expandOpen]: this.state.expanded,
								})}
								onClick={this.handleExpandClick}
								aria-expanded={this.state.expanded}
								aria-label="Show more"
								variant="contained"
								href="#contained-buttons"
								className={styles.button}
							>
								{this.state.buttonTitle}
							</Button>
						</CardActions>
						<Collapse in={this.state.expanded} timeout="auto" unmountOnExit>
							<CardContent>
								<Typography variant='title'>
									Plot
								</Typography>
								<Typography align='justify' paragraph>
									{this.props.searchedMovie.Plot}
								</Typography>

								<div style={{display:"flex"}}>
									<Typography variant='body2'>
										Director:
									</Typography>
									<Typography style={{marginLeft:10}} >
										{this.props.searchedMovie.Director}
									</Typography>
								</div>

								<div style={{display:"flex"}}>
									<Typography variant='body2'>
										Writer:
									</Typography>
									<Typography style={{marginLeft:10}}>
										{this.props.searchedMovie.Writer}
									</Typography>
								</div>

								<div style={{display:"flex"}}>
									<Typography variant='body2'>
										Starring:
									</Typography>
									<Typography style={{marginLeft:10}}>
										{this.props.searchedMovie.Actors}
									</Typography>
								</div>

								<div style={{display:"flex"}}>
									<Typography variant='body2'>
										Production, company:
									</Typography>
									<Typography style={{marginLeft:10}}>
										{this.props.searchedMovie.Production}
									</Typography>
								</div>

								<div style={{display:"flex"}}>
									<Typography variant='body2'>
										Released:
									</Typography>
									<Typography style={{marginLeft:10}}>
										{this.props.searchedMovie.Released}
									</Typography>
								</div>

								<div style={{display:"flex"}}>
									<Typography variant='body2'>
										Running time:
									</Typography>
									<Typography style={{marginLeft:10}}>
										{this.props.searchedMovie.Runtime}
									</Typography>
								</div>

								<div style={{display:"flex"}}>
									<Typography variant='body2'>
										Country:
									</Typography>
									<Typography style={{marginLeft:10}}>
										{this.props.searchedMovie.Country}
									</Typography>
								</div>

								<div style={{display:"flex"}}>
									<Typography variant='body2'>
										Language:
									</Typography>
									<Typography style={{marginLeft:10}}>
										{this.props.searchedMovie.Language}
									</Typography>
								</div>
								
								{this.props.searchedMovie.Ratings.map(rating => (
									<div style={{display:"flex"}}>
										<Typography variant='body2'>
											{rating.Source}:
										</Typography>
										<Typography style={{marginLeft:10}}>
											{rating.Value}
										</Typography>
									</div>
								))}
							</CardContent>
						</Collapse>
				</Card>;
		else
			display = 
				<Card className={styles.card}>
					<CardHeader title={this.props.searchedMovie.Error} />
				</Card>;

		return(display);
  	}
}

