import React, { Component } from 'react';
import Search from './Search.js'
import SearchResults from './SearchResults.js' 
import './App.css';

export default class App extends Component {
	constructor(props, context){
    	super(props);
    	this.state = {searchedMovieInfo: {}};
    	this.callbackToApi = this.callbackToApi.bind(this);
	}

	callbackToApi = movieData => {this.setState({searchedMovieInfo: movieData})}

	render() {
    	return(
			<div>
	    		<Search callback={this.callbackToApi}/>
	    		<SearchResults searchedMovie={this.state.searchedMovieInfo}/>
        	</div>
    	); 
	}
}
